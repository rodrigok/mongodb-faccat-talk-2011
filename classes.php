<?php
	class TTime
	{
		public $initialTime = 0;
		public $finalTime = 0;
		
		public function start()
		{
			$this->initialTime = microtime(true);
		}
		
		public function stop()
		{
			$this->finalTime = microtime(true);
			
			return $this->getTime();
		}
		
		public function getTime()
		{
			return $this->finalTime - $this->initialTime;
		}
	}
	
	class TMongo
	{
		public $conn;
		public $db;
		public $collection;
		
		public function __construct()
		{
			$this->conn = new Mongo();
			
			$this->db = $this->conn->mongo;
			
			$this->collection = $this->db->data;
			$this->collection->drop();
		}
		
		public function insert($obj)
		{
			$this->collection->insert($obj);
		}
	}
	
	class TMysql
	{
		public $conn;
		
		public function __construct()
		{
			$this->conn = mysql_connect('127.0.0.1', 'root', '');
			
			mysql_select_db('mongo', $this->conn);
			
			$sql = "DROP TABLE IF EXISTS data;";
			mysql_query($sql);
			
			$sql = " CREATE TABLE mongo.data ("
						."id int NOT NULL,"
						."nome varchar(100) NOT NULL,"
						."sobrenome varchar(100) NOT NULL);";
			
			mysql_query($sql);
		}
		
		public function insert($sql)
		{
			mysql_query($sql);
		}
	}
	
	class TPostgres
	{
		public $conn;
		public $db;
		public $collection;
		
		public function __construct()
		{
			$this->conn = pg_pconnect('host=localhost port=5432 dbname=mongo user=postgres password=postgres');
			
			$sql = "DROP TABLE IF EXISTS data;";
			pg_query($this->conn, $sql);
			
			$sql = " CREATE TABLE data ("
						."id int NOT NULL,"
						."nome character varying(100) NOT NULL,"
						."sobrenome character varying(100) NOT NULL);";
			
			pg_query($this->conn, $sql);
		}
		
		public function insert($sql)
		{
			pg_query($this->conn, $sql);
		}
	}
	
?>