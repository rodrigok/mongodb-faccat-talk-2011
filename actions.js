use demo;

db.help();

db.getCollectionNames();

db.pessoas.help;

db.pessoas.drop();
while (db.pessoas.count() < 10) { db.pessoas.insert({nome: 'João', idade: db.pessoas.count()}) }
while (db.pessoas.count() < 20) { db.pessoas.insert({nome: 'Ronaldo', sobrenome: 'Silva'}) }

db.pessoas.find()

db.pessoas.find({nome: /o/, idade: {$gt: 2, $lt: 7}})

a = db.pessoas.findOne();
a.idade = 10;
db.pessoas.save(a);
db.pessoas.findOne();

db.produtos.drop();
db.produtos.insert({nome: 'Livro1', categorias: ['Livro', 'Biografia']})
db.produtos.insert({nome: 'Livro2', categorias: ['Livro', 'Ficção']})

db.produtos.find({categorias: 'Biografia'});