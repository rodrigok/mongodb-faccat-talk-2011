Ext.require('Ext.chart.*');
Ext.require(['Ext.Window', 'Ext.fx.target.Sprite', 'Ext.layout.container.Fit']);

Ext.onReady(function () {
	var win;
	
	
	
	var store = Ext.create('Ext.data.JsonStore', {
		proxy: {
			type: 'ajax',
			url: 'index.php',
			timeout: 90000,
			reader: {
				type: 'json'
			}
		},
		fields: ['db', 'time', 'timeFind', 'count']
	});
	
	
	
	win = Ext.create('Ext.Window', {
		width: 800,
		height: 300,
		minHeight: 400,
		minWidth: 550,
		hidden: false,
		maximizable: true,
		maximized: true,
		title: 'Bar Chart',
		//renderTo: Ext.getBody(),
		listeners: {
			render: function(){
				var mask = new Ext.LoadMask(this.body, {msg:"Carregando..."});
				
				store.on('beforeload', function(){
					mask.show();
				});
				store.on('load', function(){
					mask.hide();
				});
				store.load();
			}
		},
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		defaults: {
			flex: 1
		},
		tbar: [{
			text: 'Reload Data',
			handler: function() {
				store.load();
			}
		}],
		items: [{
			id: 'chartCmp',
			xtype: 'chart',
			animate: true,
			shadow: true,
			store: store,
			axes: [{
				type: 'Numeric',
				position: 'bottom',
				fields: ['time'],
				title: 'Time',
				grid: true,
				minimum: 0
			}, {
				type: 'Category',
				position: 'left',
				fields: ['db'],
				title: 'Data Base'
			}],
			background: {
			  gradient: {
				id: 'backgroundGradient',
				angle: 45,
				stops: {
				  0: {
					color: '#ffffff'
				  },
				  100: {
					color: '#eaf1f8'
				  }
				}
			  }
			},
			series: [{
				type: 'bar',
				axis: 'bottom',
				highlight: true,
				tips: {
				  trackMouse: true,
				  width: 200,
				  height: 28,
				  renderer: function(storeItem, item) {
					this.setTitle(storeItem.get('db') + ': ' + storeItem.get('time'));
				  }
				},
				label: {
				  display: 'insideEnd',
					field: 'time',
					orientation: 'horizontal',
					color: '#333',
					'text-anchor': 'middle'
				},
				xField: 'db',
				yField: ['time']
			}]
		},{
			xtype: 'chart',
			animate: true,
			shadow: true,
			store: store,
			axes: [{
				type: 'Numeric',
				position: 'bottom',
				fields: ['timeFind'],
				title: 'time',
				grid: true,
				minimum: 0,
				maximum: 0.1
			}, {
				type: 'Category',
				position: 'left',
				fields: ['db'],
				title: 'Data Base'
			}],
			background: {
			  gradient: {
				id: 'backgroundGradient',
				angle: 45,
				stops: {
				  0: {
					color: '#ffffff'
				  },
				  100: {
					color: '#eaf1f8'
				  }
				}
			  }
			},
			series: [{
				type: 'bar',
				axis: 'bottom',
				highlight: true,
				tips: {
				  trackMouse: true,
				  width: 200,
				  height: 28,
				  renderer: function(storeItem, item) {
					this.setTitle(storeItem.get('db') + ': ' + storeItem.get('count'));
				  }
				},
				label: {
				  display: 'insideEnd',
					field: 'timeFind',
					orientation: 'horizontal',
					color: '#333',
					'text-anchor': 'middle'
				},
				xField: 'db',
				yField: ['timeFind']
			}]
		}]
	}).show();
});
