<?php
	//ini_set('display_errors', 'true');
	//ini_set('show_errors', 'E_ALL');
	set_time_limit(30000);

	require('classes.php');
	
	$total = 10000;
	$result = array();
	
	$ttime = new TTime();
	$tmongo = new TMongo();
	$tmysql = new TMysql();
	$tpostgres = new TPostgres();
	
	$ttime->start();
	for($i = 0; $i < $total; $i++)
	{
		$obj = array(
			"id" => $i,
			"nome" => "Rodrigo",
			"sobrenome" => "Nascimento"
		);
		$tmongo->insert($obj);
	}
	$mongoTime = $ttime->stop();
	
	$ttime->start();
	for($i = 0; $i < $total; $i++)
	{
		$sql = 	" INSERT INTO data (id, nome, sobrenome) values(".$i.", 'Rodrigo', 'Nascimento')";
		
		$tmysql->insert($sql);
	}
	$mysqlTime = $ttime->stop();
	
	$ttime->start();
	for($i = 0; $i < $total; $i++)
	{
		$sql = 	" INSERT INTO data (id, nome, sobrenome) values(".$i.", 'Rodrigo', 'Nascimento')";
		
		$tpostgres->insert($sql);
	}
	$postgreslTime = $ttime->stop();
	
	
	
	
	
	$ttime->start();
	$filter = array("nome" => "Rodrigo");
	$cursor = $tmongo->collection->find($filter);
	$c = $cursor->count(true);
	
	$result[] = array(
		'db' => 'Mongo',
		'time' => $mongoTime,
		'timeFind' => $ttime->stop(),
		'count' => $c
	);
	
	$ttime->start();
	$sql = "SELECT * FROM data WHERE nome = 'Rodrigo'";
	$r = mysql_query($sql);
	$c = mysql_num_rows($r);
	
	$result[] = array(
		'db' => 'Mysql',
		'time' => $mysqlTime,
		'timeFind' => $ttime->stop(),
		'count' => $c
	);
	
	$ttime->start();
	$sql = "SELECT * FROM data WHERE nome = 'Rodrigo'";
	$r = pg_query($tpostgres->conn, $sql);
	$c = count(pg_fetch_all($r));
	
	$result[] = array(
		'db' => 'PostgreSQL',
		'time' => $postgreslTime,
		'timeFind' => $ttime->stop(),
		'count' => $c
	);
	
	echo json_encode($result);
?>